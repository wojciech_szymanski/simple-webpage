<HTML>

<HEAD>
    <TITLE>Wiejskie Jadlo</TITLE>
    <meta http-equiv="content-type" content="text/html" charset="iso-8859-2">
    <link rel="stylesheet" href="style.css">
	<script type = "text/javascript" src = "scripts/addToCart.js"></script>
</HEAD>

<BODY>

    <?php session_start(); ?>
    
    <?php include 'scripts/login_link.php'; ?>

    <h2> Koszyk </h2>
    <div id = "cart">
    	<?php include 'scripts/list_cart.php'; ?>
    </div>

    <div id = "txtHint"></div>

    <?php include 'scripts/debug.php'; ?>

</BODY>
</HTML>
