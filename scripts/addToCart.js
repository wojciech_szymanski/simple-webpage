function addToCart() {
	const urlParams = new URLSearchParams(window.location.search);
	const myParam = urlParams.get('id');
	var xhttp;
	xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("txtHint").innerHTML = this.responseText;
		}
	};
	xhttp.open("GET", "scripts/addToCart.php?id="+myParam, true);
	xhttp.send();
}

function reloadCart() {
	window.open("https://students.mimuw.edu.pl/~ws406380/zamowienia.php","_self")
}

function confirmOrder() {
	var xhttp;
	xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("txtHint").innerHTML = this.responseText;
			setTimeout(function(){
			    reloadCart();
			}, 800);
		}
	};
	xhttp.open("GET", "scripts/confirmOrder.php", true);
	xhttp.send();
}
