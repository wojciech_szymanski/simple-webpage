<html>
<head>
</head>
<body>
<script>
    setTimeout(function(){
        window.open("https://students.mimuw.edu.pl/~ws406380/logowanie.php","_self")
    }, 2000);
</script>
<?php
    $name = $_POST['NAME'];
    $pass = $_POST['PASW'];
    $surn = $_POST['SURN'];
    $street = $_POST['STREET'];
    $city = $_POST['CITY'];
    $zip = $_POST['ZIP'];
    $email = $_POST['EMAIL'];

    if (empty($name) || empty($pass) || empty($surn) || empty($street) || empty($city) || empty($zip) || empty($email)) {
        echo "Nie dodano konta; wszystkie pola musza byc wypelnione.</body></html>";
        return;
    }

    $conn = oci_connect("ws406380","x");
    if (!$conn) {
        echo "oci_connect failed\n";
        $e = oci_error();
        echo $e['message'];
    }

    $query = "INSERT INTO clients(name, surname, pass, street, city, zipcode, email) VALUES (:name_bv, :surn_bv, :pass_bv, :street_bv, :city_bv, :zip_bv, :email_bv)";
    $add = oci_parse($conn, $query);
    oci_bind_by_name($add, ":name_bv", $name);
    oci_bind_by_name($add, ":surn_bv", $surn);
    oci_bind_by_name($add, ":pass_bv", md5($pass));
    oci_bind_by_name($add, ":street_bv", $street);
    oci_bind_by_name($add, ":city_bv", $city);
    oci_bind_by_name($add, ":zip_bv", $zip);
    oci_bind_by_name($add, ":email_bv", $email);
    if (oci_execute($add, OCI_NO_AUTO_COMMIT)) {
        echo "Dodano konto";
        oci_commit($conn);
    }
    else {
        echo "Nie dodano konta; ten adres email już ma swoje konto.";
    }
?>
</body>
</html>
