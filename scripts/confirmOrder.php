<?php

    session_start();

    $user = $_SESSION['LOGIN'];
    
    // Nawiazywanie polaczenia z baza danych; login i haslo do studenckiego oracla; serwer bazodanowy jest domyslny. 
    $conn = oci_connect("ws406380","x");
    if (!$conn) {
    	echo "oci_connect failed\n";
    	$e = oci_error();
    	echo $e['message'];
    }

    //get new order ID
    $order_id_string = "SELECT orders_sequence.NEXTVAL FROM dual";
    $order_id_action = oci_parse($conn, $order_id_string);
    oci_execute($order_id_action, OCI_NO_AUTO_COMMIT);
    $order_id_return = oci_fetch_array($order_id_action, OCI_BOTH);
    $next_id = $order_id_return['NEXTVAL'];

    $insert_string = "INSERT INTO orders(order_id, client_id, order_time, status) VALUES ( :next_id_bv, :user_bv, CURRENT_TIMESTAMP(0), 'received')";
    $insert_action = oci_parse($conn, $insert_string);
    oci_bind_by_name($insert_action, ":user_bv", $user);
    oci_bind_by_name($insert_action, ":next_id_bv", $next_id);
    oci_execute($insert_action, OCI_NO_AUTO_COMMIT);

    $what_is_string = "SELECT * FROM meals_in_basket WHERE client_id = :user_bv";
    $what_is = oci_parse($conn, $what_is_string);
    oci_bind_by_name($what_is, ":user_bv", $user);
    oci_execute($what_is, OCI_NO_AUTO_COMMIT);

	while (($meal = oci_fetch_array($what_is, OCI_BOTH))) {
		$how_much = $meal['QUANTITY'];
        $meal_id = $meal['MEAL_ID'];

        $insert_string = "INSERT INTO meals_in_orders(order_id, meal_id, quantity) VALUES (:next_id_bv, :meal_id_bv, :how_much_bv)";
        $insert_action = oci_parse($conn, $insert_string);
        oci_bind_by_name($insert_action, ":next_id_bv", $next_id);
        oci_bind_by_name($insert_action, ":meal_id_bv", $meal_id);
        oci_bind_by_name($insert_action, ":how_much_bv", $how_much);
        $result = oci_execute($insert_action, OCI_NO_AUTO_COMMIT);

        //$insert_string = "INSERT INTO meals_in_orders(order_id, meal_id, quantity) VALUES (" . $next_id . ", " . $meal_id . ", " . $how_much . ")";
        //echo $insert_string . "\n<br>";
	}

    $delete_string = "DELETE FROM meals_in_basket WHERE client_id = :user_bv";
    $delete_action = oci_parse($conn, $delete_string);
    oci_bind_by_name($delete_action, ":user_bv", $user);
    oci_execute($delete_action, OCI_NO_AUTO_COMMIT);

	oci_commit($conn);
?>

Dodano zamowienie.

<?php include 'debug.php'; ?>
