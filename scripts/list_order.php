<?php 

    $conn = oci_connect("ws406380","x");
    if (!$conn) {
    	echo "oci_connect failed\n";
    	$e = oci_error();
    	echo $e['message'];
    }

    $query = "SELECT * FROM orders WHERE order_id = :order_id_bv";
    $stmt = oci_parse($conn, $query);
    oci_bind_by_name($stmt, ":order_id_bv", $_GET['id']);
    oci_execute($stmt, OCI_NO_AUTO_COMMIT);
    $order = oci_fetch_array($stmt, OCI_BOTH);

    if ($order['CLIENT_ID'] != $_SESSION['LOGIN']) {
        echo "Brak dostepu.";
        return;
    }
    
    echo "ID zamowienia: " . $order['ORDER_ID'] . "<BR><BR>Data: \n" . $order['ORDER_TIME'] . "<BR><BR>Status: \n" . $order['STATUS'] ."<BR><BR>\n";

    $query = "SELECT name, quantity, price * quantity / 100 pr FROM meals_in_orders NATURAL JOIN meals WHERE order_id = :order_id_bv";
    $stmt = oci_parse($conn, $query);
    oci_bind_by_name($stmt, ":order_id_bv", $_GET['id']);
    oci_execute($stmt, OCI_NO_AUTO_COMMIT);

    $isAnything = 0;

    while (($row = oci_fetch_array($stmt, OCI_BOTH))) {
        if ($isAnything == 0) {
            $isAnything = 1;
            echo "<table>\n<tr>
            <th>Nazwa</th>
            <th>Liczba</th>
            <th>Cena</th>
            </tr>";
        }
        echo "<tr><td>" . $row['NAME'] . "</td><td>" . $row['QUANTITY'] . "</td><td>" . $row['PR'] . "</td></tr>\n";
    }

    if ($isAnything == 1)
        echo "</table>\n";
    else
        echo "Zamowienie jest puste.";
?>