<?php

    $conn = oci_connect("ws406380","x");
    if (!$conn) {
        echo "oci_connect failed\n";
        $e = oci_error();
        echo $e['message'];
    }

    if (empty($_SESSION['LOGIN'])) {
        echo "Koszyk jest pusty. Zaloguj sie, aby dodac do koszyka.";
        return;
    }

    $query = "SELECT name, quantity, price * quantity / 100 pr FROM meals_in_basket NATURAL JOIN meals WHERE client_id = :client_id_bv";
    $stmt = oci_parse($conn, $query);
    oci_bind_by_name($stmt, ":client_id_bv", $_SESSION['LOGIN']);
    oci_execute($stmt, OCI_NO_AUTO_COMMIT);

    $isAnything = 0;

    while (($row = oci_fetch_array($stmt, OCI_BOTH))) {
        if ($isAnything == 0) {
            $isAnything = 1;
            echo "<table>\n<tr>
            <th>Nazwa</th>
            <th>Liczba</th>
            <th>Cena</th>
            </tr>";
        }
        echo "<tr><td>" . $row['NAME'] . "</td><td>" . $row['QUANTITY'] . "</td><td>" . $row['PR'] . "</td></tr>\n";
    }

    if ($isAnything == 1) {
        echo "</table>\n";
        echo "<br><button onclick = \"confirmOrder()\">Zamow</button>";    }
    else {
        echo "Koszyk jest pusty.";
    }
?>

