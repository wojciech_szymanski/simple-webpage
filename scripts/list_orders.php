<?php

    $conn = oci_connect("ws406380","x");
    if (!$conn) {
        echo "oci_connect failed\n";
        $e = oci_error();
        echo $e['message'];
    }

    if (empty($_SESSION['LOGIN'])) {
        echo "Brak zamowien. Zaloguj sie, aby dodac zamowienie.";
        return;
    }

    $query = "SELECT * FROM orders WHERE client_id = :client_id_bv ORDER BY order_id DESC";
    $stmt = oci_parse($conn, $query);
    oci_bind_by_name($stmt, ":client_id_bv", $_SESSION['LOGIN']);
    oci_execute($stmt, OCI_NO_AUTO_COMMIT);

    $any = 0;    

    while (($row = oci_fetch_array($stmt, OCI_BOTH))) {
        if ($any == 0) {
            $any = 1;
            echo "<table>\n<tr>
            <th>ID zamowienia</th>
            <th>Data zamowienia</th>
            <th>Status</th>
            </tr>";
        }
        echo "<tr><td><a href = \"zamowienie.php?id=" . $row['ORDER_ID'] ."\">" . $row['ORDER_ID'] . "</a></td><td>" . $row['ORDER_TIME'] . "</td><td>" . $row['STATUS'] . "</td></tr>\n";
    }
    if ($any == 1)
        echo "</table>\n";
    else
        echo "Brak zamowien."
?>