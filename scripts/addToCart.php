<?php

    session_start();

    if (empty($_SESSION['LOGIN'])) {
        echo "Zaloguj sie, aby dodac do koszyka.";
        return;
    }

    $user = $_SESSION['LOGIN'];
    $meal_id = $_GET['id'];
    
    // Nawiazywanie polaczenia z baza danych; login i haslo do studenckiego oracla; serwer bazodanowy jest domyslny. 
    $conn = oci_connect("ws406380","x");
    if (!$conn) {
    	echo "oci_connect failed\n";
    	$e = oci_error();
    	echo $e['message'];
    }

    $what_is = oci_parse($conn, "SELECT quantity FROM meals_in_basket WHERE client_id = :user_bv AND meal_id = :meal_id_bv");
    oci_bind_by_name($what_is, ":user_bv", $user);
    oci_bind_by_name($what_is, ":meal_id_bv", $meal_id);
	oci_execute($what_is, OCI_NO_AUTO_COMMIT);
	
    $how_much_was = 0;
	if ($prev = oci_fetch_array($what_is, OCI_BOTH)) {
		$how_much_was = $prev['QUANTITY'];

        $delete = oci_parse($conn, "DELETE FROM meals_in_basket WHERE client_id = :user_bv AND meal_id = :meal_id_bv");
        oci_bind_by_name($delete, ":user_bv", $user);
        oci_bind_by_name($delete, ":meal_id_bv", $meal_id);
		oci_execute($delete, OCI_NO_AUTO_COMMIT);
	}

    $add = oci_parse($conn, "INSERT INTO meals_in_basket VALUES (:user_bv, :meal_id_bv, :how_much_bv)");
    oci_bind_by_name($add, ":user_bv", $user);
    oci_bind_by_name($add, ":meal_id_bv", $meal_id);
    oci_bind_by_name($add, ":how_much_bv", strval(1 + $how_much_was));
	oci_execute($add, OCI_NO_AUTO_COMMIT);
	oci_commit($conn);

    echo "Dodano do koszyka.";
?>

<?php include 'debug.php'; ?>
