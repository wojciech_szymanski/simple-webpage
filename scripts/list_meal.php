<?php 

    $conn = oci_connect("ws406380","x");
    if (!$conn) {
    	echo "oci_connect failed\n";
    	$e = oci_error();
    	echo $e['message'];
    }

    $query = "SELECT * FROM meals WHERE meal_id = :meal_id_bv";
    $stmt = oci_parse($conn, $query);
    oci_bind_by_name($stmt, ":meal_id_bv", $_GET['id']);
    oci_execute($stmt, OCI_NO_AUTO_COMMIT);
    $meal = oci_fetch_array($stmt, OCI_BOTH);
    
    echo "Danie: " . $meal['NAME'] . "<BR><BR>Opis:<BR>\n" . $meal['DESCRIPTION'] . "<BR><BR>Cena: \n" . $meal['PRICE'] / 100.0 ." zł.<BR><BR> Czas przygotawania: \n" . $meal['PREP_TIME'] . " minut.<BR><BR>";
?>

<script type = "text/javascript" src = "scripts/addToCart.js"></script>
<button onclick = "addToCart()"> Dodaj do koszyka </button>
<div id = "txtHint"></div>
