DROP TABLE meals_in_orders;
DROP TABLE meals_in_basket;
DROP TABLE orders;
DROP TABLE clients;
DROP TABLE meals;

DROP SEQUENCE orders_sequence;

CREATE TABLE meals (
	name NVARCHAR2(32) NOT NULL,
	description NVARCHAR2(512) NOT NULL,
	price INTEGER NOT NULL,
	prep_time INTEGER NOT NULL,
	available NUMBER(1) NOT NULL,
	meal_id NUMBER GENERATED ALWAYS AS IDENTITY PRIMARY KEY
);

CREATE TABLE clients (
	name VARCHAR(32) NOT NULL,
	surname VARCHAR(32) NOT NULL,
	pass VARCHAR(32) NOT NULL,
	street VARCHAR(32) NOT NULL,
	city VARCHAR(32) NOT NULL,
	zipcode VARCHAR(32) NOT NULL,
	email VARCHAR(32) NOT NULL PRIMARY KEY
);

CREATE SEQUENCE orders_sequence;

CREATE TABLE orders (
	order_id NUMBER PRIMARY KEY,
	client_id VARCHAR(32) NOT NULL REFERENCES clients,
	order_time TIMESTAMP(0) NOT NULL,
	status VARCHAR(32) NOT NULL CHECK (status IN ('in_progress','ready','received','canceled'))
);

CREATE TABLE meals_in_orders (
	order_id INTEGER NOT NULL REFERENCES orders,
	meal_id INTEGER NOT NULL REFERENCES meals,
	quantity INTEGER NOT NULL CHECK (quantity > 0),
	PRIMARY KEY (order_id, meal_id)
);

CREATE TABLE meals_in_basket (
	client_id VARCHAR(32) NOT NULL REFERENCES clients,
	meal_id INTEGER NOT NULL REFERENCES meals,
	quantity INTEGER NOT NULL CHECK (quantity > 0),
	PRIMARY KEY (client_id, meal_id)
);

INSERT INTO meals(name, description, price, prep_time, available) VALUES ('Schabowy', 'Klasyczne polskie danie, schabowy, ziemniaki i zestaw surowek.', 1899, 30, 1);
INSERT INTO meals(name, description, price, prep_time, available) VALUES ('Rosol', 'Rosol z trzech mies z lanymi kluskami domowej roboty.', 799, 10, 1);
INSERT INTO meals(name, description, price, prep_time, available) VALUES ('Bigos', 'Bigos z kielbasa i miesem wolowym, podawany z pieczywem.', 1099, 20, 1);
INSERT INTO meals(name, description, price, prep_time, available) VALUES ('Pomidorowa', 'Zupa pomidorowa z ryzem.', 599, 5, 1);
INSERT INTO meals(name, description, price, prep_time, available) VALUES ('Mielony', 'Kotlet mielony z miesa wolowego, podawany z ziemniakami i surowka z marchwi.', 1099, 15, 1);
COMMIT;






